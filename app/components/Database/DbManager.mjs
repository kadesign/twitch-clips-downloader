import mysql from 'mysql2/promise';
import Logger from '@kadesign/microlog';

import { to, checkEnvVariables } from '../../utils/Helper.mjs';

import * as Queries from './Queries.mjs';
import Clip from '../../models/Clip.mjs';

export default class DbManager {
  constructor() {
    checkEnvVariables(['DB_HOST', 'DB_USER', 'DB_PASS', 'DB_NAME']);

    this._connectionOpts = {
      host: process.env.DB_HOST,
      user: process.env.DB_USER,
      password: process.env.DB_PASS,
      database: process.env.DB_NAME
    }

    this.logger = new Logger().setModuleName('DbManager').setOutputDateInUTC();
  }

  async insertClip(twitchId, slug, title, broadcaster, game, viewCount, thumbnailTwitchUrl, vodTwitchUrl, createdAt) {
    await this._singleRequest(Queries.insertStmts.insertClip,
      [twitchId, slug, title, broadcaster, game, viewCount, thumbnailTwitchUrl, vodTwitchUrl, createdAt]);
  }

  async updateClipMeta(title, viewCount, twitchId) {
    await this._singleRequest(Queries.updateStmts.updateClipMeta,
      [title, viewCount, twitchId]);
  }

  async updateClipThumbnailLocalPath(thumbnailLocalPath, twitchId) {
    await this._singleRequest(Queries.updateStmts.updateClipThumbnailLocalPath,
      [thumbnailLocalPath, twitchId]);
  }

  async updateClipVodLocalPath(vodLocalPath, twitchId) {
    await this._singleRequest(Queries.updateStmts.updateClipVodLocalPath,
      [vodLocalPath, twitchId]);
  }

  async updateClipNotAvailable(twitchId) {
    await this._singleRequest(Queries.updateStmts.updateClipNotAvailable,
      [twitchId]);
  }

  async getClipsNotDownloaded() {
    const clips = await this._singleRequest(Queries.selectStmts.getClipsNotDownloaded);
    if (clips && Array.isArray(clips) && clips.length > 0) {
      const clipsArray = [];
      clips.forEach(clip => {
        clipsArray.push(new Clip({
          id: clip.twitchId,
          slug: clip.slug,
          thumbnailUrl: clip.thumbnailTwitchUrl,
          vodUrl: clip.vodTwitchUrl,
          thumbnailLocalPath: clip.thumbnailLocalPath,
          vodLocalPath: clip.vodLocalPath
        }));
      });
      return clipsArray;
    } else {
      return [];
    }
  }

  async getAllClips() {
    this.logger.log('Retrieving stored metadata');
    const clips = await this._singleRequest(Queries.selectStmts.getAllClips);
    if (clips && Array.isArray(clips) && clips.length > 0) {
      const clipsArray = [];
      clips.forEach(clip => {
        clipsArray.push(new Clip({
          id: clip.twitchId,
          title: clip.title,
          viewCount: clip.viewCount,
          thumbnailUrl: clip.thumbnailTwitchUrl,
          vodUrl: clip.vodTwitchUrl,
          thumbnailLocalPath: clip.thumbnailLocalPath,
          vodLocalPath: clip.vodLocalPath,
          isLive: clip.isLive
        }));
      });
      this.logger.log(`Metadata for ${clipsArray.length} stored clip(s) retrieved`);
      return clipsArray;
    } else {
      this.logger.log(`No clips retrieved`);
      return [];
    }
  }

  async _singleRequest(query, params = undefined) {
    let connection = await to(mysql.createConnection(this._connectionOpts));
    if (connection.error) {
      connection.error.message = 'Database error: ' + connection.error.message;
      throw connection.error;
    }

    const [response] = await connection.result.execute(query, params); // todo: make proper error handling
    await connection.result.end();
    return response;
  }
}