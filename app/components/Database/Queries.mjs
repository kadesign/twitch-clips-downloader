export const insertStmts = {
  insertClip: 'INSERT INTO clips(twitchId, slug, title, broadcaster, game, viewCount, thumbnailTwitchUrl, vodTwitchUrl, createdAt) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)'
};

export const selectStmts = {
  getAllClips: 'SELECT twitchId, title, viewCount, thumbnailTwitchUrl, vodTwitchUrl, thumbnailLocalPath, vodLocalPath, isLive FROM clips',
  getClipsNotDownloaded: 'SELECT twitchId, slug, thumbnailTwitchUrl, vodTwitchUrl, thumbnailLocalPath, vodLocalPath FROM clips WHERE (thumbnailLocalPath IS NULL OR vodLocalPath IS NULL) AND isLive = 1'
};

export const updateStmts = {
  updateClipMeta: 'UPDATE clips SET title=?, viewCount=?, isLive=1 WHERE twitchId=?',
  updateClipThumbnailLocalPath: 'UPDATE clips SET thumbnailLocalPath=? WHERE twitchId=?',
  updateClipVodLocalPath: 'UPDATE clips SET vodLocalPath=? WHERE twitchId=?',
  updateClipNotAvailable: 'UPDATE clips SET isLive=0 WHERE twitchId=?'
}