import fs from 'fs';
import path from 'path';
import fetch from 'node-fetch';
import { URL } from 'url';

export default class Downloader {
  constructor() {
    this._THUMBNAILS_ROOT = './storage/thumbnails';
    this._VODS_ROOT = './storage/vods';
  }

  downloadThumbnail(url, twitchId) {
    return this._download(url, this._THUMBNAILS_ROOT, twitchId);
  }

  downloadVod(url, twitchId, accessToken) {
    return this._download(url, this._VODS_ROOT, twitchId, accessToken);
  }

  async _download(url, target, twitchId, accessToken) {
    const extension = path.extname(url);
    const localPath = path.resolve(target, twitchId + extension);
    let response;
    if (accessToken) {
      const fetchUrl = new URL(url);
      fetchUrl.search = accessToken.toUrlSearchParams().toString();

      response = await fetch(fetchUrl, {
        method: 'GET',
        headers: {
          'Accept': '*/*',
          'Accept-Encoding': 'identity;q=1, *;q=0',
          'Accept-Language': 'ru,en;q=0.9',
          'Pragma': 'no-cache',
          'Cache-Control': 'no-cache',
          'Host': 'production.assets.clips.twitchcdn.net',
          'Range': 'bytes=0-',
          'Referer': 'https://clips.twitch.tv/',
          'Sec-Fetch-Dest': 'video',
          'Sec-Fetch-Mode': 'no-cors',
          'Sec-Fetch-Site': 'cross-site',
          'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.1.2 Safari/605.1.15'
        }
      });
    } else {
      response = await fetch(url, {
        method: 'GET'
      });
    }

    const writer = fs.createWriteStream(localPath);
    response.body.pipe(writer);

    return new Promise((resolve, reject) => {
      writer.on('finish', async () => {
        await writer.close();
        resolve(localPath);
      });
      writer.on('error', err => {
        fs.unlinkSync(localPath);
        reject(err);
      });
    });
  }
}