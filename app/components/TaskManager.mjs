import pq from 'p-queue';
import Logger from '@kadesign/microlog';

import {checkEnvVariables, to} from '../utils/Helper.mjs';
import TwitchApiWrapper from './TwitchApiWrapper.mjs';
import DbManager from './Database/DbManager.mjs';
import Downloader from './Downloader.mjs';

const PQueue = pq.default;

export default class TaskManager {
  constructor() {
    checkEnvVariables(['TWITCH_OAUTH_TOKEN', 'TWITCH_CLIENT_ID', 'TWITCH_USERNAME', 'TWITCH_USER_ID', 'TWITCH_CLIPS_QUERY_HASH', 'TWITCH_CLIPS_ACCESSTOKEN_QUERY_HASH']);

    this.apiWrapper = new TwitchApiWrapper(process.env.TWITCH_OAUTH_TOKEN, process.env.TWITCH_CLIENT_ID,
      process.env.TWITCH_USERNAME, parseInt(process.env.TWITCH_USER_ID), {
        clips: process.env.TWITCH_CLIPS_QUERY_HASH,
        accessToken: process.env.TWITCH_CLIPS_ACCESSTOKEN_QUERY_HASH
      });
    this.dbMgr = new DbManager();
    this.downloader = new Downloader();
    this.queue = new PQueue({concurrency: 10});
    this.logger = new Logger().setModuleName('TaskManager').setOutputDateInUTC();
  }

  async start() {
    await this.fetchClips();
  }

  async fetchClips() {
    const liveClips = await this.apiWrapper.getClips();
    const storedClips = await this.dbMgr.getAllClips();

    this.logger.log('Clips processing started');

    let task = await to(this._saveNewClipsMeta(liveClips, storedClips));
    if (task.error) throw task.error;

    task = await to(this._updateClipsMeta(liveClips, storedClips));
    if (task.error) throw task.error;

    task = await to(this._switchDeletedClipsState(liveClips, storedClips));
    if (task.error) throw task.error;

    task = await to(this._downloadAbsentClips());
    if (task.error) throw task.error;

    this.logger.log('Clips processing finished');
  }

  async _switchDeletedClipsState(liveClips, storedClips) {
    const availableClipsIds = this._extractIds(liveClips);
    const storedClipsIds = this._extractIds(storedClips, true);

    const deletedClipsIds = storedClipsIds.filter(id => !availableClipsIds.includes(id));
    this.logger.log(`${deletedClipsIds.length} clip(s) went down since last check`);

    for (const id of deletedClipsIds) {
      await this.queue.add(async () => {
        await to(this.dbMgr.updateClipNotAvailable(id));
      });
    }
  }

  async _saveNewClipsMeta(liveClips, storedClips) {
    const storedClipsIds = this._extractIds(storedClips);

    const newClips = liveClips.filter(clip => !storedClipsIds.includes(parseInt(clip.id)));
    this.logger.log(`${newClips.length} clip(s) were created since last check`);

    for (const clip of newClips) {
      this.logger.log(`Saving metadata for clip ${clip.slug} queued`);
      await this.queue.add(async () => {
        this.logger.log(`Saving metadata for clip ${clip.slug}`);
        await to(this.dbMgr.insertClip(clip.id, clip.slug, clip.title, clip.broadcaster, clip.game, clip.viewCount, clip.thumbnailUrl, clip.vodUrl, clip.createdAt));
        this.logger.log(`Metadata for clip ${clip.slug} saved`);
      });
    }
  }

  async _updateClipsMeta(liveClips, storedClips) {
    const storedClipsIds = this._extractIds(storedClips);

    const clips = liveClips.filter(clip => storedClipsIds.includes(parseInt(clip.id)));
    this.logger.log(`${clips.length} clip(s) is/are still live`);

    for (const clip of clips) {
      const storedClip = storedClips.filter(item => item.id === parseInt(clip.id))[0];
      if (storedClip.title !== clip.title || storedClip.viewCount !== clip.viewCount) {
        this.logger.log(`Metadata for clip ${clip.slug} was changed, updating is queued`);
        await this.queue.add(async () => {
          this.logger.log(`Updating metadata for clip ${clip.slug}`);
          await to(this.dbMgr.updateClipMeta(clip.title, clip.viewCount, clip.id));
          this.logger.log(`Metadata for clip ${clip.slug} updated`);
        });
      }
    }
  }

  async _downloadAbsentClips() {
    const clips = await this.dbMgr.getClipsNotDownloaded();
    this.logger.log(`${clips.length} clip(s) is/are not yet downloaded`);

    for (const clip of clips) {
      this.logger.log(`Downloading clip ${clip.slug} queued`);
      await this.queue.add(async () => {
        this.logger.log(`Downloading thumbnail for clip ${clip.slug}`);
        const thumbnailLocalPath = await this.downloader.downloadThumbnail(clip.thumbnailUrl, clip.id);
        await this.dbMgr.updateClipThumbnailLocalPath(thumbnailLocalPath, clip.id);
        this.logger.log(`Thumbnail for clip ${clip.slug} downloaded`);
      });
      await this.queue.add(async () => {
        this.logger.log(`Downloading clip ${clip.slug}`);
        const accessToken = await this.apiWrapper.getClipAccessToken(clip.slug);
        const vodLocalPath = await this.downloader.downloadVod(clip.vodUrl, clip.id, accessToken);
        await this.dbMgr.updateClipVodLocalPath(vodLocalPath, clip.id);
        this.logger.log(`Clip ${clip.slug} downloaded`);
      });
    }
  }

  _extractIds (clipArray, onlyLive = false) {
    if (clipArray && Array.isArray(clipArray) && clipArray.length > 0) {
      const idsArray = [];
      clipArray.forEach(clip => {
        if (onlyLive) {
          if (clip.isLive) idsArray.push(parseInt(clip.id));
        } else {
          idsArray.push(parseInt(clip.id));
        }
      });
      return idsArray;
    } else {
      return null;
    }
  }
}