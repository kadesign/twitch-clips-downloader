import fetch from 'node-fetch';
import Logger from '@kadesign/microlog';

import RequestBody from '../models/RequestBody.mjs';
import ClipsPage from '../models/ClipsPage.mjs';
import Clip from '../models/Clip.mjs';
import GetAccessTokenRequestBody from '../models/GetAccessTokenRequestBody.mjs';
import AccessToken from '../models/AccessToken.mjs';

export default class TwitchApiWrapper {
  constructor(oauthToken, clientId, username, userId, queryHashes) {
    this._uri = 'https://gql.twitch.tv/gql';
    this._headers = {
      'Accept': '*/*',
      'Host': 'gql.twitch.tv',
      'Pragma': 'no-cache',
      'Cache-Control': 'no-cache',
      'Content-Type': 'text/plain;charset=UTF-8',
      'Authorization': `OAuth ${oauthToken}`,
      'Client-Id': clientId,
      'Origin': 'https://dashboard.twitch.tv',
      'Referer': `https://dashboard.twitch.tv/u/${username}/content/clips`,
      'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.1.2 Safari/605.1.15'
    }
    this._username = username;
    this._userId = userId;
    this._queryHashes = queryHashes;
    this.logger = new Logger().setModuleName('TwitchApiWrapper').setOutputDateInUTC();
  }

  async getClips() {
    this.logger.log('Retrieving metadata for clips from Twitch');
    const body = new RequestBody(this._username, this._userId, this._queryHashes.clips);
    let responsePage;
    const clips = [];
    do {
      responsePage = await this._sendRequest(body);
      if (responsePage.clips.length > 0) {
        clips.push(...responsePage.clips);
      }
      body.setCursor(responsePage.nextPageCursor);
    } while (responsePage.hasNextPage);

    this.logger.log(`Metadata for ${clips.length} clip(s) retrieved`)
    return clips;
  }

  async getClipAccessToken(slug) {
    const body = new GetAccessTokenRequestBody(slug, this._queryHashes.accessToken);
    let response = await this._sendRequest(body);

    this.logger.log(`Access token for clip ${slug} retrieved`);
    return response;
  }

  async _sendRequest(body) {
    const apiResponse = await fetch(this._uri, {
      method: 'POST',
      headers: this._headers,
      body: body.toBodyString()
    });

    if (apiResponse.status === 200) {
      const parsedResponse = await apiResponse.json();
      if (parsedResponse[0]) {
        if (body instanceof RequestBody) {
          return this._createClipsPage(parsedResponse);
        } else if (body instanceof GetAccessTokenRequestBody) {
          return this._getAccessToken(parsedResponse);
        }
      }
    } else {
      throw new Error(`Can't fetch data from ${this._uri}: ${apiResponse.status} ${apiResponse.statusText}`);
    }
  }

  _createClipsPage(parsedResponse) {
    if (parsedResponse[0] && parsedResponse[0].data.user && parsedResponse[0].data.user.clips) {
      const clips = [];
      let cursor = null;

      parsedResponse[0].data.user.clips.edges.forEach(edge => {
        const clip = new Clip({
          id: edge.node.id,
          slug: edge.node.slug,
          title: edge.node.title,
          broadcaster: edge.node.broadcaster.login,
          game: edge.node.game ? edge.node.game.name : null,
          viewCount: edge.node.viewCount,
          thumbnailUrl: edge.node.thumbnailURL,
          vodUrl: edge.node.videoQualities[0].sourceURL,
          createdAt: edge.node.createdAt
        });
        if (!cursor && edge.cursor) cursor = edge.cursor;
        clips.push(clip);
      });
      return new ClipsPage(clips, parsedResponse[0].data.user.clips.pageInfo.hasNextPage, cursor);
    } else {
      throw new Error('Invalid response');
    }
  }

  _getAccessToken(parsedResponse) {
    if (parsedResponse[0] && parsedResponse[0].data && parsedResponse[0].data.clip) {
      return new AccessToken(parsedResponse[0].data.clip.playbackAccessToken.signature, parsedResponse[0].data.clip.playbackAccessToken.value);
    } else {
      throw new Error('Invalid response');
    }
  }
}