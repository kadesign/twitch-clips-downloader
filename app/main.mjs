import dotenv from 'dotenv';
import Logger from '@kadesign/microlog';

import TaskManager from './components/TaskManager.mjs';

dotenv.config();
const logger = new Logger().setOutputDateInUTC();

async function main() {
  const taskMgr = new TaskManager();
  await taskMgr.start()
}

main().catch(err => logger.error(err));
