import { URLSearchParams } from 'url';

export default class AccessToken {
  constructor(signature, token) {
    this.sig = signature;
    this.token = token;
  }

  toUrlSearchParams() {
    return new URLSearchParams({
      sig: this.sig,
      token: this.token
    });
  }
}