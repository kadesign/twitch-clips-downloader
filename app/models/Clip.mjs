const TWITCH_CLIPS_ROOT_URL = 'https://clips.twitch.tv/';

export default class Clip {
  constructor(meta) {
    this.id = meta.id;
    this.slug = meta.slug;
    this.title = meta.title;
    this.broadcaster = meta.broadcaster;
    /**
     *
     * @type {?string}
     */
    this.game = meta.game;
    this.viewCount = parseInt(meta.viewCount);
    this.thumbnailUrl = meta.thumbnailUrl;
    this.thumbnailLocalPath = meta.thumbnailLocalPath || null
    this.vodUrl = meta.vodUrl;
    this.vodLocalPath = meta.vodLocalPath || null;
    this.createdAt = new Date(meta.createdAt);
    this.isLive = Boolean(parseInt(meta.isLive)) || null;
  }

  get url() {
    return TWITCH_CLIPS_ROOT_URL + this.slug;
  }
}