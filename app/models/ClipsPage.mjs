export default class ClipsPage {
  constructor(clips, hasNextPage, nextPageCursor) {
    this.hasNextPage = hasNextPage;
    this.nextPageCursor = nextPageCursor;
    this.clips = clips;
  }
}