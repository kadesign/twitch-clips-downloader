export default class GetAccessTokenRequestBody {
  constructor(slug, queryHash) {
    this._body = [{
      operationName: 'VideoAccessToken_Clip',
      variables: {
        slug: slug
      },
      extensions: {
        persistedQuery: {
          version: 1,
          sha256Hash: queryHash
        }
      }
    }];
  }

  toBodyString() {
    return JSON.stringify(this._body);
  }
}