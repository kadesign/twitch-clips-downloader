export default class RequestBody {
  constructor(login, curatorID, queryHash) {
    this._body = [{
      operationName: 'ClipsManagerTable_User',
      variables: {
        login: login,
        limit: 20,
        criteria: {
          sort: 'CREATED_AT_DESC',
          period: 'ALL_TIME',
          curatorID: `${curatorID}`
        }
      },
      extensions: {
        persistedQuery: {
          version: 1,
          sha256Hash: queryHash
        }
      }
    }];
  }

  setCursor(cursor) {
    this._body[0].variables.cursor = cursor;
  }

  toBodyString() {
    return JSON.stringify(this._body);
  }
}