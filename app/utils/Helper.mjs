export const to = promise => {
  return promise.then(data => {
    return {result: data};
  })
    .catch(err => {
      return {error: err}
    });
};

export const checkEnvVariables = variables => {
  const missingEnvVars = [];
  variables.forEach(envVar => {
    if (!Object.keys(process.env).includes(envVar)) missingEnvVars.push(envVar);
  });
  if (missingEnvVars.length > 0)
    throw new Error(`Following environment variables are not set: ${missingEnvVars.join(', ')}`);
};